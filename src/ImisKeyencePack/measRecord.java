package ImisKeyencePack;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class measRecord {

	protected String idStr, descriptionStr, nominalStr, UTBStr, LTBStr, UTStr, LTStr, actualStr, deviationStr, judgeStr, displayStr, etalonStr, addStr, multiStr, refLB, refUB;
	
	NumberFormat formatter4dec = new DecimalFormat("#0.0000");
	//NumberFormat formatter3dec = new DecimalFormat("#0.000");
	
	
	public measRecord(){
		allToNull();
		
	}
	
	public measRecord(String id, String description, String nominal, String uT, String lT, String etalonValue, String addValue, String multValue) {	
		allToNull();
		
		setIdStr(id);
		setDescriptionStr(description);
		setNominal(nominal);
		setUT(uT);
		setLT(lT);
		setEtalonStr(etalonValue);
		setAddStr(addValue);
		setMultiStr(multValue);
		
	}
	
	protected void allToNull() {
		nominalStr="-";
		UTBStr="-";
		LTBStr="-";
		UTStr="-";
		LTStr="-";
		actualStr="-";
		deviationStr="-";
		judgeStr="-";
		displayStr="-";
		idStr="-";
		descriptionStr="-";
		refLB="-";
		refUB="-";
		
		
	}
	
	//---Nominal----------
 	public String getNominalStr() {
		return nominalStr;
	}
	public double getNominalAsDouble() {
		return Double.parseDouble(nominalStr.replace(",", "."));
	}
	public void setNominal(String nominalStr) {
		//this.nominalStr = nominalStr;
		this.nominalStr = formatter4dec.format(Double.parseDouble(nominalStr.replace(",", ".")));
		
		
		
		
		if (!(UTStr.equals("-")) & !(LTStr.equals("-"))) {
			setUTB(getNominalAsDouble() + getUTAsDouble());
			setLTB(getNominalAsDouble() + getLTAsDouble());	
		}
		
	}
	public void setNominal(Double nominalDouble) {
		this.nominalStr = formatter4dec.format(nominalDouble);
		
		if (!(UTStr.equals("-")) & !(LTStr.equals("-"))) {
			setUTB(getNominalAsDouble() + getUTAsDouble());
			setLTB(getNominalAsDouble() + getLTAsDouble());	
		}
	
		
	}
	
	//---UT----------------
	public String getUTStr() {
		return UTStr;
	}
	public double getUTAsDouble() {
		return Double.parseDouble(UTStr.replace(",", "."));
	}
	public void setUT(String uTStr) {
		UTStr = formatter4dec.format(Double.parseDouble(uTStr.replace(",", ".")));
		
		if (!nominalStr.equals("-") & !LTStr.equals("-")) {
			setUTB(getNominalAsDouble() + getUTAsDouble());
			setLTB(getNominalAsDouble() + getLTAsDouble());	
		}
		
	}
	public void setUT(Double uTDouble) {
		UTStr = formatter4dec.format(uTDouble).replace(",", ".");
		
		if (!nominalStr.equals("-") & !LTStr.equals("-")) {
			setUTB(getNominalAsDouble() + getUTAsDouble());
			setLTB(getNominalAsDouble() + getLTAsDouble());	
		}
		
	}

		
		
	//---LT----------------
	public String getLTStr() {
		return LTStr;
	}
	public double getLTAsDouble() {
		//return Double.parseDouble(LTStr);
		return Double.parseDouble(LTStr.replace(",", "."));
	}
	public void setLT(String lTStr) {
		LTStr = formatter4dec.format(Double.parseDouble(lTStr.replace(",", ".")));
		
		if (!nominalStr.equals("-") & !UTStr.equals("-")) {
			setUTB(getNominalAsDouble() + getUTAsDouble());
			setLTB(getNominalAsDouble() + getLTAsDouble());	
		}
		
	}
	public void setLT(Double lTDouble) {
		LTStr = formatter4dec.format(lTDouble);
		
		if (!nominalStr.equals("-") & !UTStr.equals("-")) {
			setUTB(getNominalAsDouble() + getUTAsDouble());
			setLTB(getNominalAsDouble() + getLTAsDouble());	
		}
		
	}
	
	
	//---UTB----------------
	public String getUTBStr() {
		return UTBStr;
	}
	public double getUTBAsDouble() {
		return Double.parseDouble(UTBStr);
	}
	protected void setUTB(String uTBStr) {
		UTBStr = uTBStr;
	}
	protected void setUTB(Double uTBDouble) {
		UTBStr = uTBDouble.toString();
	}
	
	
	//---LTB----------------
	public String getLTBStr() {
		return LTBStr;
	}
	public double getLTBAsDouble() {
		
		return Double.parseDouble(LTBStr);
	}
	protected void setLTB(String lTBStr) {
		LTBStr = lTBStr;
		
	}
	protected void setLTB(Double lTBDouble) {
		LTBStr = lTBDouble.toString();
	}
	
	
	
	
	
	//---Actual----------------
	public String getActualStr() {
		return actualStr;
	}
	public double getActualAsDouble() {
		return Double.parseDouble(actualStr.replace(",", "."));
	}
	public void setActual(String actualStr) {
		this.actualStr = formatter4dec.format(Double.parseDouble(actualStr));
		
		//this.actualStr = formatter4dec.format(Double.parseDouble(actualStr)*getMultiAsDouble()+getAddAsDouble());
		makeJudgement();
		setDeviation();
	}
	
	public void setActual(Double actualDouble) {
		
		this.actualStr = formatter4dec.format(actualDouble);
		if (nominalStr!= "-" & UTStr != "-" & LTStr != "-" & actualStr != "-") {
			makeJudgement();
			setDeviation();
			
			
		}
	}

	//---Deviation----------------
	public String getDeviationStr() {
		return deviationStr;
	}
	
	public double getDeviationAsDoulbe() {
		return Double.parseDouble(deviationStr);
	}
	
	protected void setDeviation(){
		if (!(nominalStr.equals("-")) & !(UTStr.equals("-")) & !(LTStr.equals("-")) & !(actualStr.equals("-"))) {
			makeJudgement();
			double dev = getActualAsDouble() - getNominalAsDouble();
			deviationStr = formatter4dec.format(dev);
		}
	}
	
	
	//---Judge----------------
 	public String getJudgeStr() {
		return judgeStr;
	}
	public boolean getJudgeAsBoolean() {
		if (getJudgeStr().equals("OK")) {
			return true;
		}else {
			return false;
		}
		
	}
	
	public boolean makeJudgement() {
		if (!nominalStr.equals("-") & !UTStr.equals("-") & !LTStr.equals("-") & !actualStr.equals("-")) {
			if (getActualAsDouble()<getLTBAsDouble()) {
				judgeStr="Not OK";
				setDisplayStr();
				return true;
			}else {
				if (getUTBAsDouble()<getActualAsDouble()) {
					judgeStr="Not OK";
					setDisplayStr();
					return true;
				}else {
					judgeStr="OK";
					setDisplayStr();
					return true;
				}
			}
		}else {
			return false;
		}
	}
	

	
	
	//---Display----------------
	public String getDisplayStr() {
		return displayStr;
	}
	public void setDisplayStr() {
		if (!nominalStr.equals("-") & !UTStr.equals("-") & !LTStr.equals("-") & !actualStr.equals("-")) {
			Double segment = (getUTBAsDouble() - getLTBAsDouble())/7;
			int category = (int) ((getActualAsDouble() - getLTBAsDouble())/segment);
			
			if (getActualAsDouble() < getLTBAsDouble()) {
				displayStr = "<-------";
			}else {
				if (getUTBAsDouble() < getActualAsDouble()) {
					displayStr = "------->";
				}else {
					switch (category){
						case 0:
							displayStr = "(|------)";
							break;
						case 1:
							displayStr = "(-|-----)";
							break;
						case 2:
							displayStr = "(--|----)";
							break;
						case 3:
							displayStr = "(---|---)";
							break;
						case 4:
							displayStr = "(----|--)";
							break;
						case 5:
							displayStr = "(-----|-)";
							break;
						case 6:
							displayStr = "(------|)";
							break;
						default:
							displayStr = "(------|)";
							break;
					}
				}
			}
		}	
	}

	
	
	
	public String getIdStr() {
		return idStr;
	}

	public void setIdStr(String idStr) {
		this.idStr = idStr;
	}

	public String getDescriptionStr() {
		return descriptionStr;
	}

	public void setDescriptionStr(String descriptionStr) {
		this.descriptionStr = descriptionStr;
	}

	
	//---Etalon �rt�k-----------
	public String getEtalonStr() {
		return etalonStr;
	}
	
	public double getEtalonAsDouble() {
		return Double.parseDouble(etalonStr.replace(",", "."));
	}

	protected void setEtalonStr(String etalonStr) {
		this.etalonStr = etalonStr;
	}
	
	protected void setEtalonStr(Double etalonStr) {
		this.etalonStr = etalonStr.toString();
	}
	
	
	//---Addit�v �rt�k-----------
	public String getAddStr() {
		return addStr;
	}
	
	public double getAddAsDouble() {
		return Double.parseDouble(addStr.replace(",", "."));
	}

	protected void setAddStr(String addStr) {
		this.addStr = addStr;
	}
	
	protected void setAddStr(Double addStr) {
		this.addStr = addStr.toString();
	}
	
	
	//---Multiplikativ �rt�k-----------
	public String getMultiStr() {
		return multiStr;
	}
	
	public double getMultiAsDouble() {
		return Double.parseDouble(multiStr.replace(",", "."));
				
	}

	protected void setMultiStr(String multiStr) {
		this.multiStr = multiStr;
	}
	
	protected void setMultiStr(Double multiStr) {
		this.multiStr = multiStr.toString();
	}

	public synchronized String getRefLB() {
		return refLB;
	}
	
	public synchronized Double getRefLBAsDouble() {
		return Double.parseDouble(refLB);
	}

	public synchronized void setRefLB(String refLB) {
		this.refLB = formatter4dec.format(Double.parseDouble(refLB.replace(",", ".")));
	}

	public synchronized String getRefUB() {
		return refUB;
	}
	
	public synchronized Double getRefUBasDouble() {
		return Double.parseDouble(refUB);
	}

	public synchronized void setRefUB(String refUB) {
		this.refUB = formatter4dec.format(Double.parseDouble(refUB.replace(",", ".")));
	}


}
