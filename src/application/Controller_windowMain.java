package application;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;



import ImisKeyencePack.keyenceDLEN1;
import ImisKeyencePack.measRecord;
import javafx.application.Platform;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class Controller_windowMain implements Initializable {
	
//*01*********Attributes/Objects********************************************************************	
	protected final String SETTINGSPATH = "settings.ini";
	protected final String NOMINALSPATH = "nominals.ini";
	protected final String REFERENCEPATH = "referencePart.ini";
	protected final String KEYENSETTINGSPATH = "keyenceSettings.ini";
	protected final String STATION_NAME = "PPM-IMT1901-V3";
	protected String refPartID;
	protected String resultsFilePath;
	protected static final long THREAD_DELTA_T = 100;
	
	protected TableColumn<measRecord, String> colID,colDesc,colNominal,colUT,colLT,colActual,colDeviation,colDisplay,colJudgement;
	protected Map<String,measRecord> mapOfMeasurements;
	protected Map<String,Double> results;
	protected keyenceDLEN1 keyenceModul = new keyenceDLEN1(KEYENSETTINGSPATH);
	protected String drawningNr, operationNr;
	protected Thread thread1, thread2;
	protected boolean judgement;
	protected boolean threadInterrupted;
	protected int maxMinutesToCalib;
	protected int remainingTime;
	protected boolean timeCheckInterrupted;
	protected Runnable timeCheck;
	protected File errorLogFile;

//*02********FXML objects********************************************************************	
	@FXML TableView<measRecord> twResults;
	@FXML Button btnPreset;
	@FXML Label lblDrawingNr;
	@FXML Label lblOperation;
	@FXML TextField tfPartID;
	@FXML ToggleButton tgbtnMeause;
	@FXML Label lblMinutesToCalib;
	@FXML Label lblStationName;

//*03********Initialize********************************************************************	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		//3.1 UI be�llt�sa
		//Be�llt�sok olvas�sa �s alkalmaz�sa
		if (!readSettings()) 	System.out.println("Be�ll�t�sokat nem siker�lt beolvasni.");
		
		remainingTime=0;
		lblMinutesToCalib.setText(remainingTime + " perc m�lva.");
		
		
		//N�vlegesek olvas�sa �s be�llt�sa
		if (!readNominals(NOMINALSPATH)) 	System.out.println("N�vlegeseket nem siker�lt beolvasni.");
		
		//Scene berendez�se
		if (!setScene()) 		System.out.println("Scene-t nem siker�lt be�ll�tani.");
		
		
		
		//3.2 Runnable �s Thread folyamatos m�r�shez
		Runnable contMeasCylce = new Runnable() {
			@Override
			public void run() {
				//thread megszakt� boolean hamis
				setThreadInterrupted(false);
				
				//ciklus a m�r�s ism�telget�s�hez
				while (!isThreadInterrupted()) {
					//M�r�s adatkimenet n�lk�l
					
					measCycle(false);
					
					//Thread altat�sa, hogy ne p�r�g�j�n annyira
					try {
						Thread.sleep(THREAD_DELTA_T);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						makeErrorLog("loc-1", e);
						e.printStackTrace();
					}
					
					
					//UI frisst�se adatokkal az operendszernek alkalmas id�poontban.
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							fillResultTable();
							
						}
						
					});
					
					//Kil�p�s ciklusb�l, ha a thread interrupt boolean igaz
					if(isThreadInterrupted()) {  //Csak hogy ne legyen m�g egy felesleges lefutas
						break;
					}
				}
			}
			
		};
		
		thread1 = new Thread (contMeasCylce);
		thread1.setName("thread1");
		thread1.setDaemon(true);
		
		timeCheck = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				while(!isTimeCheckInterrupted()) {
					
					Platform.runLater(new Runnable() {

						@Override
						public void run() {
							int i = getRemainingTime()-1;
							setRemainingTime(i);
							lblMinutesToCalib.setText(i + " perc m�lva.");
							
							if(!(i>0)) {
								setThreadInterrupted(true);
								thread1 = new Thread(contMeasCylce);
								thread1.setName("thread1");
								thread1.setDaemon(true);
								
								setTimeCheckInterrupted(true);								
								btnPreset.setDisable(false);
								tgbtnMeause.setSelected(false);
								tfPartID.setDisable(true);
							}
						}
						
					});
					try {
						Thread.sleep(60000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						makeErrorLog("loc-2", e);
						e.printStackTrace();
					}
					if(isTimeCheckInterrupted()) {
						thread2 = new Thread(timeCheck);
						thread2.setName("thread2");
						thread2.setDaemon(true);
						
						break;
					}
				}
			}
			
		};
	
		thread2 = new Thread (timeCheck);
		thread2.setName("thread2");
		thread2.setDaemon(true);
		
		
	
		
		//3.3 Object listeners
		tgbtnMeause.selectedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
				// TODO Auto-generated method stub
				if(getRemainingTime()>0) {
					if (arg2) {
						btnPreset.setDisable(true);
						tfPartID.setDisable(false);
						thread1.start();
						readNominals(NOMINALSPATH);
						setScene();
						
						
					}else {
						setThreadInterrupted(true);
						thread1 = new Thread(contMeasCylce);
						thread1.setName("thread1");
						thread1.setDaemon(true);
						btnPreset.setDisable(false);
						tfPartID.setDisable(true);
					}
				}else {
					tgbtnMeause.setSelected(false);
					Alert alert = new Alert(AlertType.CONFIRMATION);
					alert.setTitle("Kalibr�l�s lej�rt");
					alert.setContentText("Tov�bbi m�r�sek el�tt kalibr�lni kell!");
					alert.setHeaderText("");
					alert.showAndWait();
					
				}
				
				
			}
			
		});
		
		btnPreset.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				
				if (actionOfBtnPreset()) {
					
					
					Alert alert = new Alert(AlertType.CONFIRMATION);
					alert.setTitle("Kalibr�l�s");
					alert.setContentText("Sikeres kalibr�l�s.");
					alert.setHeaderText("");
					alert.showAndWait();
					
					
					setRemainingTime(maxMinutesToCalib);
					
					
					setTimeCheckInterrupted(false);
					thread2 = new Thread (timeCheck);
					thread2.setName("thread2");
					thread2.setDaemon(true);
					thread2.start();
					
					setScene();
					
					
				}else {
					remainingTime=0;
					lblMinutesToCalib.setText(remainingTime + " perc m�lva.");
					
					
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Kalibr�l�s");
					alert.setContentText("Sikertelen kalibr�l�s!");
					alert.setHeaderText("");
					alert.showAndWait();
				}
			}
		});
			
		tfPartID.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent ke) {
				// TODO Auto-generated method stub
				if (ke.getCode().equals(KeyCode.ENTER)) {
					if (actionOfMeasure()) {
						
						fillResultTable();
						//makeReport(tfPartID.getText());
						tfPartID.clear();
						//judgement=getJudgement();
						//btnMeasure.setDisable(true);
						
					}else {
						System.out.println("Nem siker�lt olvasni a m�r�sieredm�nyeket. :(");
					}
				}
			}
			
		});
		
	}

	
//*04********Methods********************************************************************
	//Gombok
	protected boolean actionOfBtnPreset() {
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Kalibr�l�s");
		alert.setContentText("1. L�p�s:\n\nTiszt�tsd meg az etalont, a referenciadarabot �s a k�sz�l�ket!");
		alert.setHeaderText("");
		alert.showAndWait();
		
		Alert alert2 = new Alert(AlertType.INFORMATION);
		alert2.setTitle("Kalibr�l�s");
		alert2.setContentText("2. L�p�s:\n\nHelyezd fel az etalont!");
		alert2.setHeaderText("");
		alert2.showAndWait();
		
		setTimeCheckInterrupted(true);
		
		keyenceModul.presetAllProbes();
		
		
		Alert alert3 = new Alert(AlertType.INFORMATION);
		alert3.setTitle("Kalibr�l�s");
		alert3.setContentText("3. L�p�s:\n\nReferenciadarabos igazol�shoz helyezd fel a K�K referencia darabot!");
		alert3.setHeaderText("");
		alert3.showAndWait();
		
		readNominals(REFERENCEPATH);
		measCycle(false);
		
		
	
		
		
		if (getJudgement()) {
		//if (true) {

			tfPartID.clear();	
			makeReport("Reference");
			
			return true;

		}else {	
			
			Alert alert4 = new Alert(AlertType.ERROR);
			alert4.setTitle("Kalibr�l�s");
			alert4.setContentText("Referenciadarabos igazol�s: NotOK\nAz eszk�z nem a megadott hibahat�ron bel�l m�rte vissza a referenciadarabot.\n Tiszt�t�s ut�n ism�teld meg a kalibr�l�st vagy �rtes�tsd a m�r�g�pm�szer�szt!");
			alert4.setHeaderText("");
			alert4.showAndWait();
			
			//UI frisst�se adatokkal az operendszernek alkalmas id�poontban.
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					setScene();
					//fillResultTable();	
					
				}
				
			});
			
			makeReport("Reference");
			
			return false;
		}	
		
	}
	
	protected boolean actionOfMeasure() {
		
		measCycle(true);
		
		return true;
	}
	
	//M�r�s
	protected void measCycle(boolean makeReport) {
	
		results = keyenceModul.readAllProbes();
		getMapOfMeasurements().get("M01").setActual(calculateM01("T4", "T5", "T1", "T2", "T3")); 
		getMapOfMeasurements().get("M02").setActual(calculateM02("T1"));
		getMapOfMeasurements().get("M03").setActual(calculateM03("T2"));
		getMapOfMeasurements().get("M04").setActual(calculateM04("T3"));
		getMapOfMeasurements().get("M05").setActual(calculateM05("T6"));
		getMapOfMeasurements().get("M06").setActual(calculateM06("T4","T7"));

		
		if (makeReport) {
			makeReport(tfPartID.getText());			
		}
		
		//tfPartID.clear();
		judgement=getJudgement();
	}
	
	protected double calculateM01(String upperProbe1, String upperProbe2, String lowerProbe1, String lowerProbe2, String lowerProbe3 ) {
		double result = 999.9999;

		double etalonValue = getMapOfMeasurements().get("M01").getEtalonAsDouble();
		double additivValue =  getMapOfMeasurements().get("M01").getAddAsDouble();
		double multiValue = getMapOfMeasurements().get("M01").getMultiAsDouble();
		
		double value1 = results.get(lowerProbe1)*multiValue;
		double value2 = results.get(lowerProbe2)*multiValue;
		double value3 = results.get(lowerProbe3)*multiValue;
		double value4 = results.get(upperProbe1)*multiValue;
		double value5 = results.get(upperProbe2)*multiValue;
		
		double fPlaneHeight = (value1 + value2 + value3)/3;
		double innerPlaneHeight = (value4 + value5)/2;
		
		
		result =  etalonValue + fPlaneHeight - innerPlaneHeight + additivValue;
	
		return result;
	}
	
	protected double calculateM02(String probeName) {
		double result = 999.999;
		double multiValue = getMapOfMeasurements().get("M02").getMultiAsDouble();
		double addiValue = getMapOfMeasurements().get("M02").getAddAsDouble();
		
		result = -1 * (results.get(probeName) * multiValue+addiValue);
		return result;
	}
	
	protected double calculateM03(String probeName) {
		double result = 999.999;
		double multiValue = getMapOfMeasurements().get("M03").getMultiAsDouble();
		double addiValue = getMapOfMeasurements().get("M03").getAddAsDouble();
		result = -1* (results.get(probeName) * multiValue + addiValue);
		
		return result;
	}
	
	protected double calculateM04(String probeName) {
		double result = 999.999;
		double multiValue = getMapOfMeasurements().get("M04").getMultiAsDouble();
		double addiValue = getMapOfMeasurements().get("M04").getAddAsDouble();
		
		result = -1 * (results.get(probeName) * multiValue+addiValue);
		return result;
	}

	protected double calculateM05(String probeName) {
		double result = 0;
		double prbValue = results.get(probeName);
		double etalonValue = getMapOfMeasurements().get("M05").getEtalonAsDouble();
		double additivValue =  getMapOfMeasurements().get("M05").getAddAsDouble();
		double multiValue = getMapOfMeasurements().get("M05").getMultiAsDouble(); 
	
		result =(etalonValue - (prbValue*multiValue)) + additivValue;

		return result;
		
		
	}
	
	protected double calculateM06(String prbOnFace, String prbInGroove) {
		double result = 999.9999;

		double etalonValue = getMapOfMeasurements().get("M06").getEtalonAsDouble();
		double additivValue =  getMapOfMeasurements().get("M06").getAddAsDouble();
		double multiValue = getMapOfMeasurements().get("M06").getMultiAsDouble();
		
		double value1 = results.get(prbOnFace)*multiValue;
		double value2 = results.get(prbInGroove)*multiValue;
				
		result =  etalonValue + value1 - value2 + additivValue;
	
		return result;
	}
	
	
	//T�bl�zat
	protected boolean initTable() {
		try {
			
			twResults.getItems().clear();
			twResults.getColumns().clear();
			
			
			colID = new TableColumn<>("ID");
			colID.setCellValueFactory(new PropertyValueFactory<>("idStr"));
			colID.setStyle("-fx-alignment: CENTER");
			
			colDesc = new TableColumn<>("Megnevez�s");
			colDesc.setCellValueFactory(new PropertyValueFactory<>("descriptionStr"));
			colDesc.setStyle("-fx-alignment: CENTER");
			
			colNominal = new TableColumn<>("Nevleges");
			colNominal.setCellValueFactory(new PropertyValueFactory<>("nominalStr"));
			colNominal.setStyle("-fx-alignment: CENTER");
			
			colUT = new TableColumn<>("FT");
			colUT.setCellValueFactory(new PropertyValueFactory<>("UTStr"));
			colUT.setStyle("-fx-alignment: CENTER");
			
			colLT = new TableColumn<>("LT");
			colLT.setCellValueFactory(new PropertyValueFactory<>("LTStr"));
			colLT.setStyle("-fx-alignment: CENTER");
			
			colActual = new TableColumn<>("M�rt");
			colActual.setCellValueFactory(new PropertyValueFactory<>("actualStr"));
			colActual.setStyle("-fx-alignment: CENTER");
			
			colDeviation = new TableColumn<>("Elt�r�s");
			colDeviation.setCellValueFactory(new PropertyValueFactory<>("deviationStr"));
			colDeviation.setStyle("-fx-alignment: CENTER");
			
			colDisplay = new TableColumn<>("Diagram");
			colDisplay.setCellValueFactory(new PropertyValueFactory<>("displayStr"));
			colDisplay.setStyle("-fx-alignment: CENTER");
			
			colJudgement = new TableColumn<>("Ok/notOK");
			colJudgement.setCellValueFactory(new PropertyValueFactory<>("judgeStr"));
			colJudgement.setStyle("-fx-alignment: CENTER");
			

			twResults.getColumns().addAll(colID,colDesc,colNominal,colUT,colLT,colActual,colDeviation,colDisplay,colJudgement);
			
		
			
			for (String chr: getMapOfMeasurements().keySet()) {
				
				twResults.getItems().add(getMapOfMeasurements().get(chr));
			
			}
			
			colID.setSortType(TableColumn.SortType.ASCENDING);
			twResults.getSortOrder().add(colID);
			

			
			return true;
			
		} catch(Exception e){
			makeErrorLog("loc-3", e);
			return false;
		}
		
		
	}
	
	protected boolean fillResultTable() {
		try {
			
			twResults.setRowFactory(tv -> new TableRow<measRecord>() {
				@Override
				public void updateItem(measRecord record, boolean empty) {
					super.updateItem(record, empty);
					
					setStyle("");
					setFont(Font.font ("Verdana",FontWeight.BOLD, 40));
					setStyle("-fx-control-inner-background-alt: -fx-control-inner-background");
					
					if (record != null ) {
						
						if (!record.getJudgeStr().equals("-")) {
							if (record.getJudgeAsBoolean()) {
								setStyle("-fx-background-color: springgreen;");
								
							}else {
								setStyle("-fx-background-color: lightpink;");
								
							}
						}
					}
				}
			});
						
			colID.setSortType(TableColumn.SortType.ASCENDING);
			twResults.getSortOrder().add(colID);
		
			
			return true;
			
		} catch(Exception e){
			makeErrorLog("loc-4", e);
			return false;
		}
		
		
	}
	
	//Be�llt�sok
 	protected boolean readNominals(String nominalsFilePath) {
 		
 		mapOfMeasurements = new HashMap<String,measRecord>();
		try {
			
			boolean read = false;
			File nominalsFile = new File (nominalsFilePath);
			Scanner sc = new Scanner(nominalsFile);
			String line;
			
			while (sc.hasNextLine()) {
				
				line = sc.nextLine();
				if (line.equals("[/nominals]")) read = false;
				if (read) {
					
					String[] data = line.split(";");
					if (data[0].equals("characteristic")) {
						measRecord newChar = new measRecord(data[1], data[2], data[3], data[4], data[5],data[6],data[7],data[8]);
						
						getMapOfMeasurements().put(newChar.getIdStr(), newChar);
						
					}
				}
				if (line.equals("[nominals]") ) read = true;
			}
			sc.close();
			
			
			
			
			return true;
		} catch (FileNotFoundException e) {
			makeErrorLog("loc-5", e);
			e.printStackTrace();
			return false;
		}
		
	}
 	
 	protected boolean readSettings() {
 		try {
 			boolean read = false;
 			
 			File settingsFile = new File (SETTINGSPATH);
			Scanner sc = new Scanner(settingsFile);
			String line;
			while (sc.hasNext()) {
				line = sc.nextLine();
				if (line.equals("[/prgSettings]")) read = false;
				if (read) {
					String[] data = line.split("=");
					if (data[0].equals("drawingNr")) drawningNr = data[1];
					if (data[0].equals("operation")) operationNr = data[1];
					if (data[0].equals("resultsFilePath")) resultsFilePath = data[1];
					if (data[0].equals("maxMinutesToCalib")) maxMinutesToCalib = Integer.parseInt(data[1]);
				}
				
				
				if (line.equals("[prgSettings]")) read = true;
			}
			
			sc.close();
			
			return true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			makeErrorLog("loc-6", e);
			e.printStackTrace();
			return false;
		}
 		
 		
 		
 		
 	}
 	
	protected boolean setScene() {
		
		
		try {
			
			lblStationName.setText(STATION_NAME);
			lblDrawingNr.setText(drawningNr);
			lblOperation.setText(operationNr);
			
			//btnPreset.setDisable(false);
		
			//tfPartID.setDisable(true);
			initTable();
			
			fillResultTable();
			
			return true;
		} catch(Exception e) {
			makeErrorLog("loc-7", e);
			e.printStackTrace();
			return false;
		}
		
		
		
		
	}
	
	//Adatkimenet
	protected boolean makeReport(String partID) {
		if (partID.length()>0) {
			try {
				//F�jl eddig tartalm�nak beolvas�sa
				List<String> prevResults = new ArrayList<String>();
				File resultFile = new File (resultsFilePath);
				Scanner sc;
				sc = new Scanner (resultFile);
				while (sc.hasNextLine()) {
					prevResults.add(sc.nextLine());
				}
				sc.close();
				
				// �j sorok hozz�ad�sa
				String header = getResultHeader();
				String newResult = getMeasAsResultRecord(partID);
				
				if (prevResults.size()>0 & (!(prevResults.get(0).contentEquals(header)))) {
					prevResults.add(0, header);					
				}
				prevResults.add(1,newResult);
				
				//F�jl �r�sa
				PrintWriter pw = new PrintWriter(resultFile);
				for (String element : prevResults) {
					pw.println(element);
				}
				pw.close();
		
			} catch (FileNotFoundException e) {
				makeErrorLog("loc-8", e);
				System.out.println("Rossz el�r�si �t kimeneti f�jlnak.");
				e.printStackTrace();
				return false;
			}
					
			return true;
		}else {
			System.out.println("Nincs megadva darabazonost�!");
			return false;
		}
	
	}
	
	protected boolean makeErrorLog(String location, Exception e) {
		
		errorLogFile = new File ("errorLog.txt");
		List<String> content = new ArrayList<String>();
		if (errorLogFile.exists()) {
			Scanner sc;
			try {
				sc = new Scanner (errorLogFile);
				while (sc.hasNextLine()) {
					content.add(sc.nextLine());
				}
				sc.close();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("F�jl nem tal�lhat�.");
				alert.setContentText("A hibanapl� nem tal�lhat�.");
				alert.setHeaderText("");
				alert.showAndWait();
				e1.printStackTrace();
			}
			
		}else {
			try {
				errorLogFile.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("�r�si hiba");
				alert.setContentText("A megadott mappa nem �rhat�\n\n" + errorLogFile.getPath());
				alert.setHeaderText("");
				alert.showAndWait();
				e1.printStackTrace();
			}
		}
		
		content.add("---------------------------------------------------------");
		content.add(LocalDate.now() +" | "+ LocalTime.now().toString().substring(0,8));
		content.add("Bug location: " + location);
		content.add("Short description:");
		content.add(e.toString());
		content.add("Full description:");
		
		
		try {
			PrintWriter pw = new PrintWriter(errorLogFile);
			for (String line : content) {
				pw.println(line);
			}
			e.printStackTrace(pw);
			pw.close();
		
		
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("�r�si hiba");
			alert.setContentText("A f�jl nem �rhat�\n\n" + errorLogFile.getPath());
			alert.setHeaderText("");
			alert.showAndWait();
		}
		
		
		
		return true;
	}
	
	
//*05********Getters/Setters********************************************************************	
	protected boolean getJudgement() {
		for (String element : getMapOfMeasurements().keySet()) {
			if (!getMapOfMeasurements().get(element).getJudgeAsBoolean()) {
				return false;
			}
		}
		return true;
	}
	
	protected String getJudgementAsString() {
		for (String element : getMapOfMeasurements().keySet()) {
			if (!getMapOfMeasurements().get(element).getJudgeAsBoolean()) {
				return "NotOK";
			}
		}
		return "OK";
	}
	
	protected synchronized boolean isTimeCheckInterrupted() {
		return timeCheckInterrupted;
	}
	
	protected synchronized void setTimeCheckInterrupted(boolean b) {
		timeCheckInterrupted=b;
	}
	
	protected synchronized boolean isThreadInterrupted() {
		return threadInterrupted;
	};
	
	protected synchronized void setThreadInterrupted(boolean b) {
		threadInterrupted = b;
	};
	
	protected synchronized Map<String,measRecord> getMapOfMeasurements(){
		return mapOfMeasurements;
	}
	
	protected String getMeasAsResultRecord(String partID) {
		String newRecord = 	LocalDate.now()								+";" +
							LocalTime.now().toString().substring(0,8)	+";"+
							partID 										+";"+
							getJudgementAsString()						+";"+
							getMapOfMeasurements().get("M01").getActualStr()	+";"+
							getMapOfMeasurements().get("M02").getActualStr()	+";"+
							getMapOfMeasurements().get("M03").getActualStr()	+";"+
							getMapOfMeasurements().get("M04").getActualStr()	+";"+
							getMapOfMeasurements().get("M05").getActualStr()	+";"+
							getMapOfMeasurements().get("M06").getActualStr();
		return newRecord;
	}
	
	protected String getResultHeader() {
		String header = "Datum;Ido;Azonosito;Minosites;M01;M02;M03;M04;M05;M06";
		return header;
	}

	public synchronized int getRemainingTime() {
		return remainingTime;
	}
	


	public synchronized void setRemainingTime(int maxMinutesToCalib) {
		this.remainingTime = maxMinutesToCalib;
	}
	
}
