package ImisKeyencePack;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;



public class keyenceDLEN1 {

	protected String IP;
	protected String port;
	
	protected String iniFilePath;
	protected LinkedList<String> settingsLines;
	protected Map<String, keyenceProbe> probes;
	protected Socket s;
	protected PrintWriter pw;
	protected InputStreamReader isr;
	protected BufferedReader bfr;
	
	
	
	public keyenceDLEN1(String iniFilePath) {
		File iniFile = new File(iniFilePath);
		
		settingsLines = getSettings(iniFile);
		
		if (!setEthernetModul(settingsLines)) 	System.out.println("Modul be�ll�t�sa nem sieker�lt!");
		
		probes = setProbes(settingsLines);
		
		try {
			s = new Socket(IP,Integer.parseInt(port));
			pw = new PrintWriter(s.getOutputStream());
			
			isr = new InputStreamReader(s.getInputStream());
			bfr = new BufferedReader(isr);
			
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			makeErrorLog("Loc-DLEN1-1", e);
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			makeErrorLog("Loc-DLEN1-2", e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			makeErrorLog("Loc-DLEN1-3", e);
		}
		
		
		
	}
	
	public HashMap<String,Double> readAllProbes() {
			
				
			
			
			HashMap<String,Double> results = new LinkedHashMap<String,Double>();
			
			
			try {

				String readCommand = "M0\r\n";

				pw.print(readCommand);
				pw.flush();
				
				
				String response = null;
				
				
				response = bfr.readLine();
				
				
				String[] data = response.split(",");
				if (data[0].equals("M0")) {
					int res =1;
					
					for (String element : probes.keySet()){
						double value = (Double.parseDouble(data[res++]))/10000;
						results.put(element, value);
					}
				}else {
					System.out.println("Ismeretlen hiba. Valasz az egys�gt�l: " + response);
					return results;
				}
				
				
				
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				makeErrorLog("Loc-DLEN1-4", e);
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				makeErrorLog("Loc-DLEN1-5", e);
				e.printStackTrace();
			}
			
			
			
			return results;
		
	}
	
	public boolean presetAllProbes() {
		String unitNumber, presetCommand, response;
		Map<String,Boolean> responses = new HashMap<String,Boolean>(); 
		for (String element : probes.keySet()) {
			
			
			try {
				unitNumber = probes.get(element).getUnit();
				presetCommand = "SW," + unitNumber + ",001,+000000001\r\n";
				
				pw.print(presetCommand);
				pw.flush();
				
				response = "";
				response = bfr.readLine();

				String[] data = response.split(",");
				
				if (data[0].equals("SW")) {
					responses.put(unitNumber, true);
				}else {
					responses.put(unitNumber, false);
					System.out.println("Nem siker�lt presetelni a tapit�t. V�lasz: " + response);
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				makeErrorLog("Loc-DLEN1-6", e);
				e.printStackTrace();
			}
			
			
			
			
		}
		
		for (String element : responses.keySet()) {
			if (responses.get(element)==false) {
				return false;
			}
		}
		
		
		return true;
	}
	
	
	protected Map<String, keyenceProbe> setProbes(List<String> settingsList){
		Map<String, keyenceProbe> result = new LinkedHashMap<String, keyenceProbe>();
		boolean read = false;
		
		String probeName = null;
		String probeUnit = null;
		char probeSlot = 0;
		
		for (String element : settingsList) {
			if (element.equals("[/Probes]")) read = false;
			if (read) {
				String[] data = element.split("=");
				if (data[0].equals("[Probe]")) {
					probeName = null;
					probeUnit = null;
					probeSlot = 0;
				}
				if (data[0].equals("name")) probeName = data[1];
				
				if (data[0].equals("slot")) {
					probeSlot = data[1].charAt(0);
					switch (probeSlot) {
					case 'A':
						probeUnit = "01";
						break;
					case 'B':
						probeUnit = "02";
						break;
					case 'C':
						probeUnit = "03";
						break;
					case 'D':
						probeUnit = "04";
						break;
					case 'E':
						probeUnit = "05";
						break;
					case 'F':
						probeUnit = "06";
						break;
					case 'G':
						probeUnit = "07";
						break;	
					}
				}
				if (data[0].equals("[/Probe]")) {
					if (probeName == null) 	{
						
						System.out.println("Probe name is missing!");
						
					} else {
						
						if (probeUnit == null) {
							
							System.out.println("Probe unit is missing!");
						
						}else {
							
							if (probeSlot == 0) {
								
								System.out.println("Probe slot is missing!");
								
							}else {
								
								keyenceProbe newProbe = new keyenceProbe(probeName,probeUnit,probeSlot,IP,port);
								
								result.put(probeName, newProbe);
								
							}
						}						
					}				
				}				
			}
			
			if (element.equals("[Probes]")) read = true;
			
		}
		
		
		return result;
		
	}
	
	protected  LinkedList<String> getSettings(File iniFile) {
		LinkedList<String> settings =  new LinkedList<String>();
		
		try {
			Scanner sc = new Scanner(iniFile);
			while (sc.hasNext()) {
				settings.add(sc.nextLine());
			}
			
			sc.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("ini file for keyence setting is missing!");
			makeErrorLog("Loc-DLEN1-6", e);
			e.printStackTrace();
		}
		
		
		
		
		return settings;
	}
	
	protected boolean setEthernetModul(List<String> settingsList) {
		boolean isIpSet = false;
		boolean isPortSet = false;
		//boolean isUnitsSet = false;
		boolean read = false;
		 
		
		for (String element : settingsList) {
			if (element.equals("[/EthernetModul]") ) {
				read = false;
				//return isIpSet & isPortSet & isUnitsSet;
			}
			if (read) {
				String[] data = element.split("=");
				
				if (data[0].equals("IP")) {
					
					setIP(data[1]);
					isIpSet = true;
				}
				
				if (data[0].equals("port")) {
					setPort(data[1]);
					
					isPortSet = true;
				}
			}
			
			if (element.equals("[EthernetModul]")) read = true;
		}
		
		
		return isIpSet & isPortSet;
	}
	
	
	protected boolean makeErrorLog(String location, Exception e) {
		
		File errorLogFile = new File ("errorLog.txt");
		List<String> content = new ArrayList<String>();
		if (errorLogFile.exists()) {
			Scanner sc;
			try {
				sc = new Scanner (errorLogFile);
				while (sc.hasNextLine()) {
					content.add(sc.nextLine());
				}
				sc.close();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("F�jl nem tal�lhat�.");
				alert.setContentText("A hibanapl� nem tal�lhat�.");
				alert.setHeaderText("");
				alert.showAndWait();
				e1.printStackTrace();
			}
			
		}else {
			try {
				errorLogFile.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("�r�si hiba");
				alert.setContentText("A megadott mappa nem �rhat�\n\n" + errorLogFile.getPath());
				alert.setHeaderText("");
				alert.showAndWait();
				e1.printStackTrace();
			}
		}
		
		content.add("---------------------------------------------------------");
		content.add(LocalDate.now() +" | "+ LocalTime.now().toString().substring(0,8));
		content.add("Bug location: " + location);
		content.add("Short description:");
		content.add(e.toString());
		content.add("Full description:");
		
		
		try {
			PrintWriter pw = new PrintWriter(errorLogFile);
			for (String line : content) {
				pw.println(line);
			}
			e.printStackTrace(pw);
			pw.close();
		
		
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("�r�si hiba");
			alert.setContentText("A f�jl nem �rhat�\n\n" + errorLogFile.getPath());
			alert.setHeaderText("");
			alert.showAndWait();
		}
		
		
		
		return true;
	}
	
	
	
	
	
	
	//---Getters and Setters----------
	
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}


	public String getIniFilePath() {
		return iniFilePath;
	}

	public void setIniFilePath(String iniFilePath) {
		this.iniFilePath = iniFilePath;
	}




	

}
