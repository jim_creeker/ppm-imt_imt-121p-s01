package ImisKeyencePack;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class keyenceProbe {
	
	protected String name;
	protected String unit;
	protected char slot;
	protected String probeNumber;
	protected String modulsIp;
	protected String modulsPort;
	
	
	public keyenceProbe(String name, String unit, char slot, String modulsIp, String modulsPort) {
		this.setName(name);
		

		this.setUnit(unit);
		this.setSlot(slot);
		this.setModulsIp(modulsIp);
		this.setModulsPort(modulsPort);
		
	}
	
	

	public boolean preset() {
		boolean isPresetSucces = false;
		int port = Integer.parseInt(modulsPort);
		
		
		try {

			String readCommand = "SW," + probeNumber + ",001,+000000001\r\n";
			
			Socket s = new Socket(modulsIp,port);
			PrintWriter pw = new PrintWriter(s.getOutputStream());
			InputStreamReader isr = new InputStreamReader(s.getInputStream());
			BufferedReader bfr = new BufferedReader(isr);

			pw.print(readCommand);
			pw.flush();
			
			
			String response = bfr.readLine();
			String assumedResponse = "SW," + probeNumber + ".001";
			if (response == assumedResponse) {
				isPresetSucces = true;
			}else {
				System.out.println("tapinto: " + probeNumber + " presetelese nem siker�lt. Valasz: " + response);
			}
			
			isr.close();
			bfr.close();
			s.close();
			
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			makeErrorLog("Loc-PrbObj-1", e);
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			makeErrorLog("Loc-PrbObj-2", e);
			e.printStackTrace();
		}
		
		
		
		return isPresetSucces;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public char getSlot() {
		return slot;
	}
	public void setSlot(char slot) {
		this.slot = slot;
	}

	public String getModulsIp() {
		return modulsIp;
	}

	public void setModulsIp(String modulsIp) {
		this.modulsIp = modulsIp;
	}

	public String getModulsPort() {
		return modulsPort;
	}

	public void setModulsPort(String modulsPort) {
		this.modulsPort = modulsPort;
	}


	public String getProbeNumber() {
		
		return probeNumber;
	}

	protected boolean makeErrorLog(String location, Exception e) {
		
		File errorLogFile = new File ("errorLog.txt");
		List<String> content = new ArrayList<String>();
		if (errorLogFile.exists()) {
			Scanner sc;
			try {
				sc = new Scanner (errorLogFile);
				while (sc.hasNextLine()) {
					content.add(sc.nextLine());
				}
				sc.close();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("F�jl nem tal�lhat�.");
				alert.setContentText("A hibanapl� nem tal�lhat�.");
				alert.setHeaderText("");
				alert.showAndWait();
				e1.printStackTrace();
			}
			
		}else {
			try {
				errorLogFile.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("�r�si hiba");
				alert.setContentText("A megadott mappa nem �rhat�\n\n" + errorLogFile.getPath());
				alert.setHeaderText("");
				alert.showAndWait();
				e1.printStackTrace();
			}
		}
		
		content.add("---------------------------------------------------------");
		content.add(LocalDate.now() +" | "+ LocalTime.now().toString().substring(0,8));
		content.add("Bug location: " + location);
		content.add("Short description:");
		content.add(e.toString());
		content.add("Full description:");
		
		
		try {
			PrintWriter pw = new PrintWriter(errorLogFile);
			for (String line : content) {
				pw.println(line);
			}
			e.printStackTrace(pw);
			pw.close();
		
		
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("�r�si hiba");
			alert.setContentText("A f�jl nem �rhat�\n\n" + errorLogFile.getPath());
			alert.setHeaderText("");
			alert.showAndWait();
		}
		
		
		
		return true;
	}
	
	
}

